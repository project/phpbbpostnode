REDAME file for the phpbbPostNode module for Drupal 5.x.

You must have CURL installed and working properly for this module to work.

The phpbbPostNode module adds three fields to the node edit/create form.

The first field allows users to check a box which will add a new topic to a forum that is decided
in the configuration settings. 

The second field will either be populated by the result of the autopost code or can be manually 
entered, if you know the topic ID. This variable can be used in your theme to provide a link to 
the newly added topic: 

number of their choice
on an existing installation of PHPBB

The administer must supply the url of the existing installation, as well as a username and password
of a user that has permissions to login to and post new topics to the forum. They must also provide
the forum number to which this new topic will be added.

The administrator can also decide what the body of the new topic will look like. The module adds a 
link to the end of the body content that references the node that created it. 
Example: "To read this article click here" where "here" is the link to the new node

*****  Installation  *****
1. You must have an installation of phpBB, this has been tested with 2.0.22

2. Change your settings.php file DB to look like this:
	$db_url['default'] = 'mysql://drupalDBUser:password@localhost/DrupalDB';
	$db_prefix = '';
	$db_url['phpbb'] = 'mysql://phpbbDBUser:password@localhost/phpbbDB';

3. Create a blank tempfile.tmp file at the root of your drupal directory for cookie storage. 
    This file must be writeable by the server

4. Copy the module folder to your modules directory and enable the module through the 
    administration panel

6. Go to the phpbbpostnode config page and set the settings.

7. You should now see the forum settings on the appropriate node type creation/edit pages

8. The first submission will not work, the cookie file has to be built first.
    To remedy this simply check the post to forum box on node creation, 
	then edit the node and check the box again. (You only have to do this once)
	
9. The variable that holds the forum topic number(number only) can be accessed via: $node->phpbbPostNode_topic
suggested use:
<?php if ($node->phpbbPostNode_topic) print '<a href="'.variable_get('phpbbPostNode_url','').'viewtopic.php?t='.$node->phpbbPostNode_topic.'">Discuss this article</a>'; ?>
